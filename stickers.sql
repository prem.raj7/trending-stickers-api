CREATE TABLE `stickers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emoji_sticker_id` bigint(20) UNSIGNED NOT NULL,
  `enable_watermark` tinyint(1) NOT NULL DEFAULT '0',
  `webp_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `png_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `start_at` timestamp NULL DEFAULT NULL,
  `end_at` timestamp NULL DEFAULT NULL,
  `type` enum('default','event','brand_campaign') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'default',
  `priority` int(10) UNSIGNED DEFAULT '1',
  `share_trackers` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `impression_trackers` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emoji_sticker_images`
--

INSERT INTO `stickers` (`id`, `emoji_sticker_id`, `enable_watermark`, `webp_image`, `png_image`, `sku`, `created_at`, `updated_at`, `start_at`, `end_at`, `type`, `priority`, `share_trackers`, `impression_trackers`) VALUES
(1, 1, 1, 'emoji-yummy-face-oreo.webp', 'emoji-yummy-face-oreo.png', NULL, '2021-02-05 09:27:07', '2021-05-15 17:23:25', '2021-02-05 20:19:00', '2021-02-18 20:19:00', 'default', 1, NULL, NULL),
(2, 2, 1, 'bigmoji-456c7a31-9bb9-e601-242c-00084ab834fd.webp', 'emoji-money-mouth-face-emoji-christmas.png', 'sku-money', '2021-02-05 09:27:07', '2021-06-07 10:01:19', NULL, NULL, 'brand_campaign', 400, '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]', '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]'),
(3, 3, 0, 'emoji-birthday-cake-oreo.webp', 'emoji-birthday-cake-oreo.png', NULL, '2021-02-05 09:27:07', '2021-04-09 05:54:31', NULL, NULL, 'default', 1, '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]', '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]'),
(4, 4, 0, 'emoji-smile-happydent.webp', 'emoji-smile-happydent.png', NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(5, 5, 1, 'bigmoji_angry_face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-04-09 05:54:49', NULL, NULL, 'default', 1, '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]', '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]'),
(6, 6, 1, 'bigmoji_lip_kiss-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(7, 7, 1, 'bigmoji_hearty_eyes-christmas-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-04-09 05:54:56', NULL, NULL, 'default', 1, '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]', '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]'),
(8, 8, 1, 'bigmoji_okay-christmas-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(9, 9, 1, 'bigmoji_sleepy_face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(10, 10, 1, 'bigmoji_sunglasses-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(11, 11, 1, 'bigmoji_thinking_face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(12, 12, 1, 'bigmoji_wow_face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(13, 13, 0, 'kissing-heart-bigmoji-cadbury.webp', 'kissing-heart-bigmoji-cadbury.png', NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(14, 14, 1, 'bigmoji_thumbsup-christmas-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(15, 15, 1, 'bigmoji_namaste-christmas-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(16, 16, 1, 'bigmoji_rofl-laughing-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(17, 17, 1, 'bigmoji_thehorns-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(18, 18, 1, 'bigmoji_stuckouttongueclosedeyes-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(19, 19, 1, 'bigmoji_flushed-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(20, 20, 1, 'bigmoji_rose-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(21, 21, 1, 'bigmoji_kissingclosedeyes-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(22, 22, 1, 'bigmoji_v-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(23, 23, 1, 'bigmoji_scream-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(24, 24, 1, 'bigmoji_night-with-stars-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(25, 25, 1, 'bigmoji_bouquet-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(26, 26, 1, 'bigmoji_facewithraisedeyebrow-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(27, 27, 1, 'bigmoji_sunrise-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(28, 28, 1, 'bigmoji_astonished-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(29, 29, 1, 'bigmoji_cry-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(30, 30, 1, 'bigmoji_middlefinger-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(31, 31, 1, 'bigmoji_chocolate-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(32, 32, 1, 'bigmoji_heartpulse-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(33, 33, 1, 'bigmoji_seenoevil-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(34, 34, 1, 'bigmoji_nomouth-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(35, 35, 1, 'bigmoji_sunriseovermountains-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(36, 36, 1, 'bigmoji_crown-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(37, 37, 1, 'bigmoji_pointright-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(38, 38, 1, 'bigmoji_rooster-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(39, 39, 1, 'bigmoji_whitefrowningface-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(40, 40, 1, 'bigmoji_hearts-1-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(41, 41, 1, 'bigmoji_neutralface-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(42, 42, 1, 'bigmoji_zzz-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(43, 43, 1, 'bigmoji_risinghand_female-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(44, 44, 1, 'bigmoji_call-me-hand-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(45, 45, 1, 'bigmoji_sunwithface-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(46, 46, 1, 'bigmoji_crescent-moon-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(47, 47, 1, 'bigmoji_sparklingheart-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(48, 48, 1, 'bigmoji_tada-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(49, 49, 1, 'bigmoji_ghost-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(50, 50, 1, 'bigmoji_100-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(51, 51, 1, 'bigmoji_disappointed-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(52, 52, 1, 'bigmoji_gift-heart-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(53, 53, 1, 'bigmoji_the-heart-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(54, 54, 1, 'bigmoji_clap-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(55, 55, 1, 'bigmoji_shushingface-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(56, 56, 1, 'bigmoji_blueheart-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(57, 57, 1, 'bigmoji_upsidedownface-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(58, 58, 1, 'bigmoji_stuckouttongue-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(59, 59, 1, 'bigmoji_iloveyouhandsign-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(60, 60, 1, 'bigmoji_risinghand_male-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(61, 61, 1, 'bigmoji_relieved-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(62, 62, 1, 'bigmoji_expressionless-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(63, 63, 1, 'bigmoji_muscle-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(64, 64, 1, 'bigmoji_boom-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(65, 65, 1, NULL, 'onam-canoe-emoji.png', NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(66, 66, 1, 'bigmoji_balloon-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(67, 67, 1, 'bigmoji_hugging-face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(68, 68, 1, 'bigmoji_smiley-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(69, 69, 1, 'bigmoji_inamused-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(70, 70, 1, 'bigmoji_smiling-face-with-3-hearts-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(71, 71, 1, 'bigmoji_blush-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(72, 72, 1, 'bigmoji_innocent-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(73, 73, 1, 'bigmoji_smirk-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(74, 74, 1, 'bigmoji_broken-heart-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(75, 75, 1, 'bigmoji_joy-christmas-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(76, 76, 1, 'bigmoji_star-struck-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(77, 77, 1, 'bigmoji_cupcake-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(78, 78, 1, 'bigmoji_laughing-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(79, 79, 1, 'bigmoji_stuck-out-tongue-winking-eye.gif-39241fa8.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(80, 80, 1, 'bigmoji_face-with-hand-over-mouth-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(81, 81, 1, 'bigmoji_partying-face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(82, 82, 1, 'bigmoji_sweat-smile-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(83, 83, 1, 'bigmoji_face-with-rolling-eyes-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(84, 84, 1, 'bigmoji_the-heart_emoji-39241fa8.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(85, 85, 1, 'bigmoji_fire-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(86, 86, 1, 'bigmoji_pleading-face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(87, 87, 1, 'bigmoji_two-hearts-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(88, 88, 1, 'bigmoji_grinning-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(89, 89, 1, 'bigmoji_relaxed-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(90, 90, 1, 'bigmoji_wave-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(91, 91, 0, 'heart-bigmoji-cadbury.webp', 'heart-bigmoji-cadbury.png', NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(92, 92, 1, 'bigmoji_revolving-hearts-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(93, 93, 1, 'bigmoji_wink-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(94, 94, 1, 'bigmoji_heartbeating-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(95, 95, 1, 'bigmoji_slightly-smiling-face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(96, 96, 1, 'bigmoji_zany-face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(97, 97, 1, 'bigmoji_flag-of-india-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(98, 98, 1, 'bigmoji_heavy-heart-exclamation-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(99, 99, 1, 'bigmoji_pensive-face-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(100, 100, 1, 'bigmoji_clinking-beer-mugs-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(101, 101, 1, 'bigmoji_sleepy-3f770ac22043.webp', NULL, NULL, '2021-02-05 09:27:07', '2021-02-05 09:27:07', NULL, NULL, 'default', 1, NULL, NULL),
(102, 1, 1, NULL, 'bigmoji-1c5c903a-7491-5586-fb4a-5a714e3c46ee.png', NULL, '2021-02-05 14:54:11', '2021-04-09 05:29:40', NULL, NULL, 'brand_campaign', 300, '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]', '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]'),
(103, 1, 1, NULL, 'bigmoji-aca36f58-1aa7-f634-4793-90c5c73c3ebd.png', NULL, '2021-02-05 14:54:42', '2021-02-05 14:54:42', NULL, NULL, 'default', 1, NULL, NULL),
(104, 4, 1, 'bigmoji-3c1f9eff-ba26-3c76-af22-96f5ad4c9dca.webp', 'bigmoji-29b5e1df-d731-ce5a-2c25-c193ba80c36f.png', NULL, '2021-02-08 08:44:31', '2021-02-08 09:43:16', '2021-02-08 14:13:00', '2021-02-09 14:14:00', 'event', 200, NULL, NULL),
(105, 104, 0, NULL, 'bigmoji-55e1bdf6-09a4-55a6-d84a-06b186acedd5.png', NULL, '2021-02-08 09:46:17', '2021-02-08 09:46:17', NULL, NULL, 'default', 1, NULL, NULL),
(106, 2, 1, 'bigmoji-21a0e0e9-a603-c6f6-c0db-ef694a2c426f.webp', NULL, NULL, '2021-02-08 11:17:01', '2021-04-09 05:29:40', NULL, NULL, 'brand_campaign', 300, '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]', '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]'),
(107, 2, 1, 'bigmoji-5580df29-748f-8d88-a59a-3935d9daea17.webp', NULL, NULL, '2021-02-08 11:19:41', '2021-04-09 05:29:40', NULL, NULL, 'brand_campaign', 300, '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]', '[{\"type\":\"direct_url\",\"url\":\"https://tps.doubleverify.com/visit.jpg?ctx=818052&cmp=DV170563&sid=BobbleAI&plc=20210407001&adsrv=0&btreg=&btadsrv=&crt=&tagtype=&dvtagver=6.1.img&\"}]'),
(108, 1, 0, 'bigmoji-2a24a934-7f15-a536-123a-7705eb8bb500.webp', NULL, NULL, '2021-05-15 16:05:06', '2021-05-15 16:05:06', NULL, NULL, 'event', 200, NULL, NULL),
(109, 2, 0, NULL, 'bigmoji-ce5df684-5e09-8528-2707-933cef17830d.png', NULL, '2021-06-07 10:52:13', '2021-06-07 10:52:13', '2021-06-09 10:37:00', '2021-06-12 10:42:00', 'default', 1, NULL, NULL),
(110, 2, 0, NULL, 'bigmoji-da6c136d-ae66-18f1-222e-58ff5b987f87.png', 'sku-money-1', '2021-06-07 10:53:32', '2021-06-07 10:53:32', '2021-06-09 10:37:00', '2021-06-12 10:42:00', 'default', 1, NULL, NULL),
(111, 2, 1, 'bigmoji-495ef7f3-a9f6-116c-e8a7-628a36a82862.webp', 'bigmoji-ffa76511-58df-f479-f93f-642880c15a27.png', 'abc', '2021-06-07 10:57:13', '2021-06-07 11:28:51', NULL, NULL, 'default', 1, NULL, NULL);
