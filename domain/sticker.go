package domain

import (
	"time"
)

//struct for getting the data from DB
type Sticker struct {
	ID        int64     `json:"id"`
	Text      string    `json:"text"`
	Priority  int       `json:"priority"`
	WEBPImage string    `json:"webpImage"`
	PNGImage  string    `json:"pngImage"`
	JPGImage  string    `json:"jpgImage"`
	UpdatedAt time.Time `json:"updatedAt"`
	CreatedAt time.Time `json:"createdAt"`
}

//embedded stucts
type ImageURL struct {
	URL            string `json:"url"`
	WithoutTextURL string `json:"withoutTextURL"`
}
type FixedWidth struct {
	Width  int      `json:"width"`
	Height int      `json:"height"`
	PNG    ImageURL `json:"png"`
	JPG    ImageURL `json:"jpg"`
	WEBP   ImageURL `json:"webp"`
}

//struct for sending the data to the user
type TrendingSticker struct {
	ID               int64      `json:"id"`
	Text             string     `json:"text"`
	URL              string     `json:"url"`
	FixedWidthTiny   FixedWidth `json:"fixedWidthTiny"`
	FixedWidthMedium FixedWidth `json:"fixedWidthMedium"`
}

//sticker usecase interface
type StickerUsecase interface {
	//usecase fucntion for fetching the trendingStickers
	GetStickers(limit int, page int) ([]TrendingSticker, error)
}

//sticker repository contract interface
type StickerRepository interface {
	//repo fucntion for fetching the trendingStickers
	GetStickers(limit int, page int) ([]Sticker, error)
}
