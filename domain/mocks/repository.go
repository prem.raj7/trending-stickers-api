package mocks

import (
	"github.com/prematid/StickerAPI/domain"
	"github.com/stretchr/testify/mock"
)

//mock type for StickerRepository
type StickerRepository struct {
	mock.Mock
}

//provides a mock function with given fields: limit, page
func (m *StickerRepository) GetStickers(limit int, page int) ([]domain.Sticker, error) {
	arguments := m.Called(limit, page)

	var r0 []domain.Sticker
	if getMockSticker, ok := arguments.Get(0).(func(int, int) []domain.Sticker); ok {
		r0 = getMockSticker(limit, page)
	} else {
		if arguments.Get(0) != nil {
			r0 = arguments.Get(0).([]domain.Sticker)
		}
	}

	var r1 error
	if getMockSticker, ok := arguments.Get(1).(func(int, int) error); ok {
		r1 = getMockSticker(limit, page)
	} else {
		r1 = arguments.Error(1)
	}

	return r0, r1
}
