package mocks

import (
	"github.com/prematid/StickerAPI/domain"
	"github.com/stretchr/testify/mock"
)

// Mock type for the StickerUsecase type
type StickerUsecase struct {
	mock.Mock
}

// Provides a mock function with given fields: limit, page
func (m *StickerUsecase) GetStickers(limit int, page int) ([]domain.TrendingSticker, error) {
	arguments := m.Called(limit, page)

	var r0 []domain.TrendingSticker
	if getMockTrendingSticker, ok := arguments.Get(0).(func(int, int) []domain.TrendingSticker); ok {
		r0 = getMockTrendingSticker(limit, page)
	} else {
		if arguments.Get(0) != nil {
			r0 = arguments.Get(0).([]domain.TrendingSticker)
		}
	}

	var r1 error
	if getMockTrendingSticker, ok := arguments.Get(1).(func(int, int) error); ok {
		r1 = getMockTrendingSticker(limit, page)
	} else {
		r1 = arguments.Error(1)
	}

	return r0, r1
}
