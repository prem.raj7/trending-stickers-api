package main

import (
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"

	"github.com/prematid/StickerAPI/config"
	"github.com/prematid/StickerAPI/stickers/delivery/http"
	"github.com/prematid/StickerAPI/stickers/repository/mysql"
	"github.com/prematid/StickerAPI/stickers/usecase"
)

var (
	DB *gorm.DB
	e  *echo.Echo
)

func init() {
	//initialising echo context
	e = echo.New()

	//middleware initialization
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())
}

//main function
func main() {
	// create a new echo instance

	var err error
	config.GetDBConfig()
	//create db connection
	DB, err = gorm.Open(config.DatabaseConfig.DBtype, config.DatabaseConfig.DBURL)

	if err != nil {
		log.Panic(err)
	}
	defer DB.Close()
	fmt.Println("Database Connection Successful")

	st := mysql.NewMysqlStickerRepository(DB)

	su := usecase.NewStickerUsecase(st)
	http.NewStickerHandler(e, su)

	// Start server
	e.Logger.Fatal(e.Start(viper.GetString(`port`)))

}
