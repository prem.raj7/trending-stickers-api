package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type DBConfig struct {
	DBUser     string
	DBPassword string
	DBName     string
	DBHost     string
	DBPort     int
	DBtype     string
	DBURL      string
}

var DatabaseConfig DBConfig

func GetDBConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("../")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
	DatabaseConfig = DBConfig{
		DBUser:     viper.GetString(`database.user`),
		DBPassword: viper.GetString(`database.pass`),
		DBName:     viper.GetString(`database.name`),
		DBHost:     viper.GetString(`database.host`),
		DBPort:     viper.GetInt(`database.port`),
		DBtype:     viper.GetString(`database.driver`),
	}
	GetDBURL()
}

func GetDBURL() {
	DatabaseConfig.DBURL = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		DatabaseConfig.DBUser,
		DatabaseConfig.DBPassword,
		DatabaseConfig.DBHost,
		DatabaseConfig.DBPort,
		DatabaseConfig.DBName)

}
