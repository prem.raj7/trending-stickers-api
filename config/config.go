package config

const (
	Limit        = 5
	Page         = 0
	TinyHeight   = 200
	TinyWidth    = 200
	MediumHeight = 450
	MediumWidth  = 450
)
