package usecase

import (
	"github.com/prematid/StickerAPI/domain"
)

type stickerUsecase struct {
	StickerRepo domain.StickerRepository
}

func NewStickerUsecase(a domain.StickerRepository) domain.StickerUsecase {
	return &stickerUsecase{
		StickerRepo: a,
	}
}

func (s *stickerUsecase) GetStickers(limit int, page int) (trendingStickers []domain.TrendingSticker, err error) {
	var stickers []domain.Sticker
	trendingStickers = []domain.TrendingSticker{}

	stickers, err = s.StickerRepo.GetStickers(limit, page)
	if err != nil {
		panic(err)
	}

	for i := range stickers {
		trendingStickers = append(trendingStickers, TrendingSticker(stickers[i]))
	}
	return trendingStickers, err
}
