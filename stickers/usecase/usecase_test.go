package usecase_test

import (
	"testing"
	"time"

	"github.com/prematid/StickerAPI/domain"
	"github.com/prematid/StickerAPI/domain/mocks"
	"github.com/prematid/StickerAPI/stickers/usecase"
	"github.com/stretchr/testify/assert"
)

func TestGetStickers(t *testing.T) {
	mockStickerRepo := new(mocks.StickerRepository)
	mockSticker := domain.Sticker{
		ID:        1,
		Text:      "Text1",
		Priority:  1,
		WEBPImage: "random1.webp",
		PNGImage:  "random1.png",
		JPGImage:  "random1.jpg",
		UpdatedAt: time.Now(),
		CreatedAt: time.Now(),
	}

	mockListStickers := make([]domain.Sticker, 0)
	mockListStickers = append(mockListStickers, mockSticker)

	t.Run("success", func(t *testing.T) {

		limit := 5
		page := 1
		mockStickerRepo.On("GetStickers", int(limit), int(page)).Return(mockListStickers, nil).Once()

		ucase := usecase.NewStickerUsecase(mockStickerRepo)

		list, err := ucase.GetStickers(limit, page)

		assert.NoError(t, err)
		assert.Len(t, list, len(mockListStickers))

		//mock assertion
		mockStickerRepo.AssertExpectations(t)

	})

}
