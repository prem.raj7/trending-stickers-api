package usecase

import (
	"github.com/prematid/StickerAPI/config"
	"github.com/prematid/StickerAPI/domain"
	"github.com/spf13/viper"
)

type URL struct {
	TinyPNGURL              string
	TinyPNGWithoutTextURL   string
	MediumPNGURL            string
	MediumPNGWithoutTextURL string

	TinyJPGURL              string
	TinyJPGWithoutTextURL   string
	MediumJPGURL            string
	MediumJPGWithoutTextURL string

	TinyWEBPURL              string
	TinyWEBPWithoutTextURL   string
	MediumWEBPURL            string
	MediumWEBPWithoutTextURL string
}

// var (
// 	mainURL                            = viper.GetString(`stickers.contents.url`)
// 	fixedWidthWaterMarkURL             = viper.GetString(`stickers.fixedWidthWaterMark.url`)
// 	fixedWidthWaterMarkWithoutTextURL  = viper.GetString(`stickers.fixedWidthWaterMarkWithoutText.url`)
// 	jpgTinyURL                         = viper.GetString(`stickers.jpg.size.tiny.url`)
// 	jpgMediumURL                       = viper.GetString(`stickers.jpg.size.medium.url`)
// 	pngTinyURL                         = viper.GetString(`stickers.png.size.tiny.url`)
// 	pngMediumURL                       = viper.GetString(`stickers.png.size.medium.url`)
// 	webpTinyURL                        = viper.GetString(`stickers.webp.size.tiny.url`)
// 	webpMediumURL                      = viper.GetString(`stickers.webp.size.medium.url`)
// )

//function for mapping the data to be returned to the request
func TrendingSticker(S domain.Sticker) domain.TrendingSticker {

	trendingStickers := domain.TrendingSticker{}

	trendingStickers.ID = S.ID
	trendingStickers.Text = S.Text
	trendingStickers.URL = ""
	trendingStickers.FixedWidthTiny.Height = config.TinyHeight
	trendingStickers.FixedWidthTiny.Width = config.TinyWidth
	trendingStickers.FixedWidthMedium.Height = config.MediumHeight
	trendingStickers.FixedWidthMedium.Width = config.MediumWidth

	url := GetURL(S)
	trendingStickers.FixedWidthTiny.PNG.URL = url.TinyPNGURL
	trendingStickers.FixedWidthTiny.PNG.WithoutTextURL = url.TinyPNGWithoutTextURL

	trendingStickers.FixedWidthMedium.PNG.URL = url.MediumPNGURL
	trendingStickers.FixedWidthMedium.PNG.WithoutTextURL = url.MediumPNGWithoutTextURL

	trendingStickers.FixedWidthTiny.JPG.URL = url.TinyJPGURL
	trendingStickers.FixedWidthTiny.JPG.WithoutTextURL = url.TinyJPGWithoutTextURL

	trendingStickers.FixedWidthMedium.JPG.URL = url.MediumJPGURL
	trendingStickers.FixedWidthMedium.JPG.WithoutTextURL = url.MediumJPGWithoutTextURL

	trendingStickers.FixedWidthTiny.WEBP.URL = url.TinyWEBPURL
	trendingStickers.FixedWidthTiny.WEBP.WithoutTextURL = url.TinyWEBPWithoutTextURL

	trendingStickers.FixedWidthMedium.WEBP.URL = url.MediumWEBPURL
	trendingStickers.FixedWidthMedium.WEBP.WithoutTextURL = url.MediumWEBPWithoutTextURL

	return trendingStickers

}

//maps the url to a public struct
func GetURL(d domain.Sticker) URL {

	mainURL := viper.GetString(`stickers.baseURL`)
	stickerURL := viper.GetString(`stickers.contents.url`)
	fixedWidthWaterMarkURL := viper.GetString(`stickers.fixedWidthWaterMark.url`)
	fixedWidthWaterMarkWithoutTextURL := viper.GetString(`stickers.fixedWidthWaterMarkWithoutText.url`)
	jpgTinyURL := viper.GetString(`stickers.jpg.size.tiny.url`)
	jpgMediumURL := viper.GetString(`stickers.jpg.size.medium.url`)
	pngTinyURL := viper.GetString(`stickers.png.size.tiny.url`)
	pngMediumURL := viper.GetString(`stickers.png.size.medium.url`)
	webpTinyURL := viper.GetString(`stickers.webp.size.tiny.url`)
	webpMediumURL := viper.GetString(`stickers.webp.size.medium.url`)

	url := URL{
		TinyPNGURL:            mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkWithoutTextURL + "/" + pngTinyURL + "/" + d.PNGImage,
		TinyPNGWithoutTextURL: mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkURL + "/" + pngTinyURL + "/" + d.JPGImage,

		MediumPNGURL:            mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkURL + "/" + pngMediumURL + "/" + d.PNGImage,
		MediumPNGWithoutTextURL: mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkWithoutTextURL + "/" + pngMediumURL + "/" + d.PNGImage,

		TinyJPGURL:            mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkWithoutTextURL + "/" + jpgTinyURL + "/" + d.JPGImage,
		TinyJPGWithoutTextURL: mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkWithoutTextURL + "/" + jpgTinyURL + "/" + d.JPGImage,

		MediumJPGURL:             mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkURL + "/" + jpgMediumURL + "/" + d.JPGImage,
		MediumJPGWithoutTextURL:  mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkWithoutTextURL + "/" + jpgMediumURL + "/" + d.JPGImage,
		TinyWEBPURL:              mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkURL + "/" + webpTinyURL + "/" + d.WEBPImage,
		TinyWEBPWithoutTextURL:   mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkWithoutTextURL + "/" + webpTinyURL + "/" + d.WEBPImage,
		MediumWEBPURL:            mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkURL + "/" + webpMediumURL + "/" + d.WEBPImage,
		MediumWEBPWithoutTextURL: mainURL + "/" + stickerURL + "/" + fixedWidthWaterMarkWithoutTextURL + "/" + webpMediumURL + "/" + d.WEBPImage,
	}
	return url
}
