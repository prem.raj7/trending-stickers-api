package mysql

import (
	"github.com/jinzhu/gorm"

	"github.com/prematid/StickerAPI/domain"
)

type mysqlStickerRepository struct {
	Conn *gorm.DB
}

func NewMysqlStickerRepository(Conn *gorm.DB) domain.StickerRepository {
	return &mysqlStickerRepository{Conn}
}

func (s *mysqlStickerRepository) GetStickers(limit int, page int) ([]domain.Sticker, error) {

	stickers := []domain.Sticker{}
	offset := (page - 1) * limit

	if err := s.Conn.Limit(limit).Offset(offset).Order("priority, id").Find(&stickers).Error; err != nil {
		return nil, err
	}

	return stickers, nil
}
