package mysql_test

import (
	"testing"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/prematid/StickerAPI/domain"
	"github.com/prematid/StickerAPI/stickers/repository/mysql"
	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var db *gorm.DB

func TestGetStickers(t *testing.T) {
	//initialising mock db
	_, mock, err := sqlmock.NewWithDSN("sqlmock_db_0")
	if err != nil {
		panic("Got an unexpected error.")
	}

	db, err = gorm.Open("sqlmock", "sqlmock_db_0")
	if err != nil {
		panic("Got an unexpected error.")
	}

	defer db.Close()

	mockSticker := []domain.Sticker{
		{
			ID:        1,
			Text:      "Text1",
			Priority:  1,
			WEBPImage: "random1.webp",
			PNGImage:  "random1.png",
			JPGImage:  "random1.jpg",
			UpdatedAt: time.Now(),
			CreatedAt: time.Now(),
		},
		{
			ID:        2,
			Text:      "Text2",
			Priority:  2,
			WEBPImage: "random2.webp",
			PNGImage:  "random2.png",
			JPGImage:  "random2.jpg",
			UpdatedAt: time.Now(),
			CreatedAt: time.Now(),
		},
	}

	rows := sqlmock.NewRows([]string{"id", "text", "priority", "webpImage", "pngImage", "jpgImage", "updatedAt", "createdAt"}).
		AddRow(mockSticker[0].ID, mockSticker[0].Text, mockSticker[0].Priority,
			mockSticker[0].WEBPImage, mockSticker[0].PNGImage, mockSticker[0].JPGImage, mockSticker[0].UpdatedAt, mockSticker[0].CreatedAt).
		AddRow(mockSticker[0].ID, mockSticker[0].Text, mockSticker[0].Priority,
			mockSticker[0].WEBPImage, mockSticker[0].PNGImage, mockSticker[0].JPGImage, mockSticker[0].UpdatedAt, mockSticker[0].CreatedAt)

	query := `SELECT \* FROM "stickers" ORDER BY priority, id LIMIT 5 OFFSET 0`

	limit := 5
	page := 1
	mock.ExpectQuery(query).WillReturnRows(rows)
	a := mysql.NewMysqlStickerRepository(db)

	list, err := a.GetStickers(limit, page)

	assert.NoError(t, err)
	assert.Len(t, list, 2)
}
