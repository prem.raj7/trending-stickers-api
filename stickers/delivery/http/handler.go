package http

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/prematid/StickerAPI/domain"
)

type stickerHandler struct {
	StickerUsecase domain.StickerUsecase
}

func NewStickerHandler(e *echo.Echo, us domain.StickerUsecase) {
	handler := &stickerHandler{StickerUsecase: us}

	e.GET("/v1/trendingStickers", handler.GetSticker)
}

var (
	err error
)

func (s *stickerHandler) GetSticker(c echo.Context) error {

	limit := GetLimit(c)
	page := GetPage(c)
	trendingStickers, _ := s.StickerUsecase.GetStickers(limit, page)

	if err != nil {
		return c.JSON(http.StatusNotFound, err)
	}
	return c.JSON(http.StatusOK, trendingStickers)
}
