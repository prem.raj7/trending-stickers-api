package http

import (
	"strconv"

	"github.com/labstack/echo"
	config "github.com/prematid/StickerAPI/config"
)

//helper function to retrieve limit
func GetLimit(c echo.Context) int {
	limit := config.Limit
	limitValue := c.QueryParam("limit")
	if limitValue != "" {
		limit, err = strconv.Atoi(limitValue)

	}
	return int(limit)
}

//helper function to retrieve page
func GetPage(c echo.Context) int {
	page := config.Page
	pageValue := c.QueryParam("page")
	if pageValue != "" {
		page, err = strconv.Atoi(pageValue)

	}
	return int(page)
}
