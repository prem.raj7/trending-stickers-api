package http

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/bxcodec/faker"
	"github.com/labstack/echo"
	"github.com/prematid/StickerAPI/domain"
	"github.com/prematid/StickerAPI/domain/mocks"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetStickers(t *testing.T) {

	var mockSticker domain.TrendingSticker
	err := faker.FakeData(&mockSticker)

	assert.NoError(t, err)
	mockUseCase := new(mocks.StickerUsecase)
	mockListStickers := make([]domain.TrendingSticker, 0)
	mockListStickers = append(mockListStickers, mockSticker)
	limit := 5
	page := 1

	mockUseCase.On("GetStickers", int(limit), int(page)).Return(mockListStickers, nil).Once()

	e := echo.New()

	req, err := http.NewRequest(echo.GET, "/v1/trendingStickers?limit=5&page=1", strings.NewReader(""))
	assert.NoError(t, err)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	handler := stickerHandler{
		StickerUsecase: mockUseCase,
	}
	err = handler.GetSticker(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusOK, rec.Code)
	mockUseCase.AssertExpectations(t)

}
